# Remarket Bot for Econobit

__*a remarket bot for Econobit, using binance as counterpart.*__

## Prerequisites
- git
- node.js (v14 or higher)

## Getting Started
---
### clone the git repository:
```bash
git clone git@bitbucket.org:econobit/remarket-exchange.git
cd remarket-exchange
```
### install dependencies and build:
```bash
npm i
npm run build
```

### run first time to create a config.json example:
```bash
npm start
```
the result should be like this:
```bash
exchange-remarket@0.1.0 start
node dist

> config file not found.
> config.json created. please, setup your api keys before run again.
```
### and finally, edit config.json with your favorite editor to setup your api keys, and run again:
```bash
npm start
```

### config.json example:

```json
{
	"strategy": {
		"spread": 0.004, // ex: 0.40%, order price spread, will be used to markup/markdown the limit order on Econobit using by reference Binance orderbook limit orders.
		"slippage": 0.002, // ex: 0.20%, order price slippage, will be cancelled and re-posted when outside slippage range.
		"fees": 0.001, // ex: 0.10%, Binance fees, as limit orders on Econobit are zero-fee.
		"amount": 0.001, // ex: 0.001 BTC, fixed amount for limit order to be posted on Econobit.
		"testnet": true // for running on livenet with real money set as false.
	},
	"binance": {
		"key": "PASTE_HERE_YOUR_BINANCE_API_KEY", // get yours on https://testnet.binance.vision
		"secret": "PASTE_HERE_YOUR_BINANCE_SECRET_KEY",
		"market": "BTC-USDT" // should be the same as econobit.market below, don't run more than one bot per account (wip).
	},
	"econobit": {
		"key": "PASTE_HERE_YOUR_ECONOBIT_API_KEY", // get yours on https://testnet.econobit.io/keys
		"secret": "PASTE_HERE_YOUR_ECONOBIT_API_SECRET",
		"market": "BTC-USDC" // should be the same as binance.market above, don't run more than one bot per account (wip).
	}
}
```

###  expected behavior:

To minimize price exposure, divide the capital in 4 equal parts (ex: 1 BTC = 100k USD):
```json
-Econobit: 25% BTC (ex: 0.25 BTC)
-Econobit: 25% USD (ex: 25k USD)
-Binance: 25% BTC (ex: 0.25 BTC)
-Binance: 25% USD (ex: 25k USD)
```

This way if the price moves up or down you will not be affected.

As you configured the fixed amount order size to be created by the bot at Econobit, it should be significantly less then the 25% (ex: 0.25 BTC) you have on your accounts at Econobit and Binance.

When you run `npm start`, bot will try to create two orders at Econobit exchange:

-limit Buy (bid) order at Econobit orderbook to buy BTC for USD, with a price marked-down by the configured spread using as reference price the Binance orderbook bid top orders.

-limit Sell (ask) order at Econobit orderbook to sell BTC for USD, with a price marked-up by the configured spread using as reference price the Binance orderbook ask orders.

The bot receive and update the Binance orderbook changes at each 100ms, the fast as possibly provided by Binance.

If the BTC price increases in USD at the Binance orderbook, the bot will look at the configured slippage rate, and if it the created limit order at Econobit goes out of the slippage range, bot will cancel the orders and re-create them at new fresh prices.

If the bid limit order created at Econobit orderbook is executed, meaning that the bot succesfully bought BTC using USD balance, the account at Econobit now has more BTC and less USD than before the execution, and the bot will instantly try to sell those BTC amount at Binance orderbook at market price, with Binance fees already considered by the config file, to sell the same BTC amount for more USD. Profit will be the extra USD inscrease at Binance compared to what was decreased in USD at Econobit.

If the ask limit order created at Econobit orderbook is executed, meaning that the bot succesfully sold BTC for USD balance, the account at Econobit now has less BTC and more USD than before the execution, and the bot will instantly try to buy those BTC amount at Binance orderbook at market price, with Binance fees already considered by the config file, to buy the same BTC amount for less USD. Profit will be the extra USD inscrease at Econobit compared to what was decreased in USD at Binance.

Depending on your capital (divided in 4 equal parts), lets assume you have 0.25 BTC at Econobit, with fixed 0.001 BTC bot orders, the bot will only be able to execute 250 times before running out of capital. So you should periodicaly re-distribute the capital in 4 equal parts again and again before any of the 4 sides run out of capital. This process is manual as it requires user-only interactions on the exchanges for the withdrawals, and cannot be automated.