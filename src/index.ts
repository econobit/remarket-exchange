import './models/state'
import { checkConfig } from './functions/check-config'
import { createConfig } from './functions/create-config'
import { loadConfig } from './functions/load-config'
import { loadRemarket } from './functions/load-remarket'

const start = async () => {
	try {
		if (!loadConfig('config.json')) {
			createConfig()
			console.log('config.json created. please, setup your api keys before run again.')
			process.exit()
		}
		if (!checkConfig()) {
			process.exit()
		}
		await loadRemarket()
	} catch (err) {
		console.error(err)
		process.exit()
	}
}

start()
