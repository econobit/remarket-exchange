import assert from 'assert'

export const deepEqual = (actual: any, expected: unknown) => {
	try {
		assert.deepStrictEqual(actual, expected)
		return true
	} catch (err) {
		if (err.name === 'AssertionError') {
			return false
		}
		throw err
	}
}
