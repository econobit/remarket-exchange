import minimist from 'minimist'
import { checkConfig } from './check-config'
import { createConfig } from './create-config'
import { loadConfig } from './load-config'
import { showHelp } from './show-help'
import { main } from '../main'
const args = minimist(process.argv.slice(2))

export const loadRemarket = async () => {
	try {
		// init
		if (args.init || args._.includes('init')) {
			createConfig()
			process.exit()
		}

		// help
		if (args.help) {
			showHelp()
			process.exit()
		}

		// load custom config file
		if (args.config) {
			if (!loadConfig(args.config)) {
				process.exit()
			}
		}

		// test
		if (args.t || args.test) {
			if (!checkConfig(args.config)) {
				process.exit()
			}
			console.log(`config file '${args.config || 'config.json'}' is valid.`)
			process.exit()
		}

		if (!checkConfig(args.config)) {
			process.exit()
		}
		await main()
	} catch (err) {
		console.error(err)
		process.exit()
	}
}
