import { state } from '../models/state'
import { loadConfig } from './load-config'

export const checkConfig = (config: string = 'config.json') => {
	if (!loadConfig(config)) {
		return false
	}
	if (state.strategy.slippage > state.strategy.spread - state.strategy.fees) {
		console.error('slippage must be equal or lower than spread minus fees')
		return false
	}
	if (state.strategy.amount <= 0) {
		console.error('amount must be greater than zero')
		return false
	}

	if (!state.econobit.key) {
		console.error('econobit key missing')
		return false
	}

	if (!state.econobit.secret) {
		console.error('econobit secret missing')
		return false
	}

	if (!state.binance.key) {
		console.error('binance key missing')
		return false
	}

	if (!state.binance.secret) {
		console.error('binance secret missing')
		return false
	}

	return true
}
