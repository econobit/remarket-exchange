import fs from 'fs'
import defaultConfig from '../config/default.json'
export const createConfig = () => {
	fs.writeFileSync('config.json', JSON.stringify(defaultConfig, null, 2))
}
