import fs from 'fs'
import path from 'path'
import defaultConfig from '../config/default.json'
import { state } from '../models/state'
export const loadConfig = (configPath: string) => {
	if (!configPath) {
		console.error('missing config path.')
		return false
	}
	if (!fs.existsSync(configPath)) {
		console.error('config file not found.')
		return false
	}
	const configFile = fs.readFileSync(path.normalize(configPath))
	const config: typeof defaultConfig = JSON.parse(configFile.toString())
	state.strategy = config.strategy
	state.econobit = {
		...state.econobit,
		...config.econobit,
	}
	state.binance = {
		...state.binance,
		...config.binance,
	}
	return true
}
