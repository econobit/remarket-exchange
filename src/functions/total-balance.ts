import Big from 'big.js'
import { state } from '../models/state'

export const totalBalances = () => {
	const econobitCoins = state.econobit.market.split('-')
	const binanceCoins = state.binance.market.split('-')

	const balances: any = {}
	for (const i in econobitCoins) {
		balances[econobitCoins[i]] =
			Big(state.econobit.balances[econobitCoins[i]].total)
				.plus(state.binance.balances[binanceCoins[i]].total)
				.toNumber()
	}

	console.log({ totalBalances: balances })
}
