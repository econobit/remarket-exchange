import fs from 'fs'
import defaultConfig from '../config/default.json'
import { state } from '../models/state'
import { createConfig } from './create-config'

export const getConfig = async (configPath?: string) => {
	if (!configPath) {
		configPath = 'config.json'
	}
	if (!fs.existsSync(configPath)) {
		configPath = 'config.json'
		createConfig()
	}
	const configFile = fs.readFileSync(configPath)
	const config: typeof defaultConfig = JSON.parse(configFile.toString())
	state.strategy = config.strategy
	state.econobit = {
		...state.econobit,
		...config.econobit,
	}
	state.binance = {
		...state.binance,
		...config.binance,
	}
}
