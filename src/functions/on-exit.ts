/* eslint-disable no-undef */

const DEFAULT_MESSAGES = {
	ctrlC: 'goodbye!',
	uncaughtException: 'Uncaught exception...'
}

interface IMessages {
	ctrlC: string
	uncaughtException: string
}
async function noCleanup() {
	return Promise.resolve(true) // signals will always terminate process
}

let messages: IMessages = null // messages to write to stderr
type ICleanupHandler = (exitCode: number, signal: NodeJS.Signals) => Promise<boolean>

let sigintHandler: NodeJS.SignalsListener // POSIX signal handlers
let sighupHandler: NodeJS.SignalsListener
let sigquitHandler: NodeJS.SignalsListener
let sigtermHandler: NodeJS.SignalsListener
let cleanupHandlers: ICleanupHandler[] = null // array of cleanup handlers to call

async function signalHandler(signal: NodeJS.Signals) {
	let exit = true

	for (const cleanupHandler of cleanupHandlers) {
		if ((await cleanupHandler(null, signal)) === false) {
			exit = false
		}
	}

	if (exit) {
		if (signal === 'SIGINT' && messages && messages.ctrlC !== '') { process.stderr.write(messages.ctrlC + '\n') }
		uninstall() // don't cleanup again
		// necessary to communicate the signal to the parent process
		process.kill(process.pid, signal)
	}
}

function exceptionHandler(e: { stack: string }) {
	if (messages && messages.uncaughtException !== '') { process.stderr.write(messages.uncaughtException + '\n') }
	process.stderr.write(e.stack + '\n')
	process.exit(1) // will call exitHandler() for cleanup
}

async function exitHandler(exitCode: number, signal: NodeJS.Signals) {
	for (const cleanupHandler of cleanupHandlers) {
		await cleanupHandler(exitCode, signal)
	}
}

export function onExit({ cleanupHandler, errors = DEFAULT_MESSAGES }: { cleanupHandler?: ICleanupHandler, errors?: IMessages }) {
	messages = errors
	if (cleanupHandlers === null) {
		cleanupHandlers = [] // establish before installing handlers

		sigintHandler = signalHandler.bind(this, 'SIGINT')
		sighupHandler = signalHandler.bind(this, 'SIGHUP')
		sigquitHandler = signalHandler.bind(this, 'SIGQUIT')
		sigtermHandler = signalHandler.bind(this, 'SIGTERM')

		process.on('SIGINT', sigintHandler)
		process.on('SIGHUP', sighupHandler)
		process.on('SIGQUIT', sigquitHandler)
		process.on('SIGTERM', sigtermHandler)
		process.on('uncaughtException', exceptionHandler)
		process.on('exit', exitHandler)

		cleanupHandlers.push(cleanupHandler || noCleanup)
	} else if (cleanupHandler) {
		cleanupHandlers.push(cleanupHandler)
	}
}

function uninstall() {
	if (cleanupHandlers !== null) {
		process.removeListener('SIGINT', sigintHandler)
		process.removeListener('SIGHUP', sighupHandler)
		process.removeListener('SIGQUIT', sigquitHandler)
		process.removeListener('SIGTERM', sigtermHandler)
		process.removeListener('uncaughtException', exceptionHandler)
		process.removeListener('exit', exitHandler)
		cleanupHandlers = null // null only after uninstalling
	}
}
onExit.uninstall = uninstall
