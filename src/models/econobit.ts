import Big from 'big.js'
import { Econobit as EconobitWS } from 'econobit'
import { IOrder, ITrade } from 'econobit/node_modules/exchange-common'
import { deepEqual } from '../functions/deep-equal'
import { totalBalances } from '../functions/total-balance'
import { state } from './state'

export class Econobit extends EconobitWS {
	ready: boolean = false
	priceCoin: string
	amountCoin: string
	onTrade: (trade: ITrade) => void
	onAddOrder: (order: IOrder) => void
	onUpdateOrder: (order: IOrder) => void
	onDeleteOrder: (orderId: number) => void
	async init() {
		this.testnet = state.strategy.testnet
		await this.connect()
		await this.login(state.econobit)
		const { list: balances, onUpdate: onUpdateBalance } = await this.subscribe.balances()
		const { onAdd: onTrade } = await this.subscribe.trades()
		const { onAdd: onAddOrder, onUpdate: onUpdateOrder, onDelete: onDeleteOrder } = await this.subscribe.openOrders()
		this.ready = true
		onTrade(trade => {
			this.onTrade(trade)
			console.log('onTrade', trade.price, trade.amount, trade.date)
		})
		onAddOrder(order => {
			this.onAddOrder(order)
			console.log('onAddOrder', order.orderId, order.price, order.amount, order.date)
		})
		onUpdateOrder((order) => {
			this.onUpdateOrder(order)
			console.log('onUpdateOrder', order.orderId, order.price, order.amount, order.filled, order.date)
		})
		onDeleteOrder((orderId) => {
			this.onDeleteOrder(orderId)
			console.log('onDeleteOrder', orderId)
		})

		const coins = state.econobit.market.split('-')
		this.amountCoin = coins[0]
		this.priceCoin = coins[1]
		for (const coin of coins) {
			const wallet = balances[coin]
			state.econobit.balances[coin] = {
				total: wallet.amount,
				free: Big(wallet.amount).minus(wallet.inOrders).toNumber(),
				used: wallet.inOrders
			}
		}
		// console.log('econobit', state.econobit.balances)
		onUpdateBalance((balance) => {
			const updatedBalance = {
				total: balance.amount,
				free: Big(balance.amount).minus(balance.inOrders).toNumber(),
				used: balance.inOrders
			}
			if (!deepEqual(updatedBalance, state.econobit.balances[balance.coin])) {
				state.econobit.balances[balance.coin] = updatedBalance
				// console.log('econobit', state.econobit.balances)
				totalBalances()
				this.ready = true
			}
		})
	}

	checkBalance(side: 'buy', price: number): boolean
	checkBalance(side: 'sell'): boolean
	checkBalance(side: 'buy' | 'sell', price?: number): boolean {
		if (Object.keys(state.econobit.balances).length === 0) {
			return false
		}
		if (side === 'buy') {
			return Big(state.econobit.balances[this.priceCoin].free).gte(Big(state.strategy.amount).mul(price))
		}
		return Big(state.econobit.balances[this.amountCoin].free).gte(state.strategy.amount)
	}

	async sendOrder({ amount, price, side }: { amount: number, price: number, side: 'buy' | 'sell' }) {
		this.ready = false
		// console.log('sendOrder', { amount, price, side })
		await this.order.send({
			side,
			amount,
			price,
			market: state.econobit.market,
			type: 'limit',
		})
	}

	async cancelOrder(orderId: number) {
		this.ready = false
		// console.log('cancelOrder', orderId)
		await this.order.cancel({ orderId })
	}

	async cancelOrders(orderIds: number[]) {
		this.ready = false
		console.log('cancelOrders', orderIds)
		await this.bulkOrders.cancel({
			orderIds
		})
	}

	async cancelAllOrders() {
		this.ready = false
		console.log('cancelAllOrders')
		await this.bulkOrders.cancel({
			all: true,
		})
	}
}
