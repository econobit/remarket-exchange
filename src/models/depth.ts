export interface IDepth {
	lastUpdateId: number
	bids: [string, string][]
	asks: [string, string][]
}
