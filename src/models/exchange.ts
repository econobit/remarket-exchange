import { IBalance } from './balance'

export interface IExchange {
	balances: { [coin: string]: IBalance },
}
