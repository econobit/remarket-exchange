import WebSocket from 'ws'
// import ccxt from 'ccxt'
export class BinanceWS extends WebSocket {
	constructor() {
		super('wss://stream.binance.com:9443/ws')
		// super('wss://testnet.binance.vision/ws')
		this.onopen = this.onOpen.bind(this)
		this.onmessage = this.onMessage.bind(this)
		this.onclose = this.onClose.bind(this)
		this.onerror = this.onError.bind(this)
	}

	public sendOp(method: string, params: string[]) {
		this.sendMsg({ method, params, id: 1 })
	}

	private onMessage(msg: WebSocket.MessageEvent) {
		if (msg.type === 'message') {
			// const data: any = JSON.parse(msg.data as string)
			// this.emit(WSEvents.data, data)
			// console.log({ data })
			return
		}
		console.error('Binance message out of format:', msg)
	}

	private onOpen() {
		// console.info('Binance connection open.')
	}

	private onError(err: WebSocket.ErrorEvent) {
		if (!err) {
			return
		}
		console.error('BinanceWS.onError:', err)
	}

	private onClose() {
		// console.info('Binance connection closed.')
	}

	/**
	 * send a message to Binance WebSocket
	 * @param msg message to send. Ex: {"op": "<command>", "args": ["arg1", "arg2", "arg3"]}
	 */
	private sendMsg(msg: any) {
		try {
			this.send(JSON.stringify(msg), (err?: Error) => {
				if (err) {
					throw err
				}
			})
		} catch (err) {
			console.error('BinanceWS.sendMsg error:', err)
		}
	}
}
