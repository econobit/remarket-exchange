import Big from 'big.js'
import { binance as BinanceApi } from 'ccxt'
import { deepEqual } from '../functions/deep-equal'
import { totalBalances } from '../functions/total-balance'
import { BinanceWS } from './binance-ws'
import { state } from './state'
export class Binance extends BinanceWS {
	ready: boolean = false
	api: BinanceApi
	priceCoin: string
	amountCoin: string
	get subscribeSymbol() {
		return state.binance.market.replace('-', '').toLowerCase()
	}

	get orderSymbol() {
		return state.binance.market.replace('-', '/').toUpperCase()
	}

	async init(subscribe: boolean = true) {
		const coins = state.binance.market.split('-')
		this.amountCoin = coins[0]
		this.priceCoin = coins[1]
		return new Promise<void>((resolve, reject) => {
			try {
				this.api = new BinanceApi({
					apiKey: state.binance.key,
					secret: state.binance.secret,
					enableRateLimit: true,
				})
				this.api.setSandboxMode(state.strategy.testnet)
				this.on('open', async () => {
					await this.balanceWatcher()
					if (subscribe) {
						this.sendOp('SUBSCRIBE', [`${this.subscribeSymbol}@depth20@100ms`])
					}
					resolve()
				})
			} catch (err) {
				reject(err)
			}
		})
	}

	async balanceWatcher() {
		await this.updateBalances()
		setTimeout(this.balanceWatcher.bind(this), 5000)
	}

	async updateBalances() {
		const balances = await this.api.fetchBalance()
		const updatedBalances = {
			[this.amountCoin]: balances[this.amountCoin],
			[this.priceCoin]: balances[this.priceCoin]
		}
		if (!deepEqual(updatedBalances, state.binance.balances)) {
			state.binance.balances = updatedBalances
			// console.log('binance', state.binance.balances)
			totalBalances()
		}
		this.ready = true
	}

	checkBalance(side: 'buy', price: number): boolean
	checkBalance(side: 'sell'): boolean
	checkBalance(side: 'buy' | 'sell', price?: number): boolean {
		if (Object.keys(state.binance.balances).length === 0) {
			return false
		}
		if (side === 'buy') {
			return Big(state.binance.balances[this.priceCoin].free).gte(Big(state.strategy.amount).mul(price))
		}
		return Big(state.binance.balances[this.amountCoin].free).gte(state.strategy.amount)
	}

	async sendOrder({ amount, price, side }: { amount: number, price: number, side: 'buy' | 'sell' }) {
		this.ready = false
		const order = await this.api.createOrder(this.orderSymbol, 'market', side, amount, price)

		const trade = order.trades[0]
		console.log('binance.sendOrder', trade.price, trade.amount, trade.cost, trade.info.commission, trade.info.commissionAsset)
		return order
	}
}
