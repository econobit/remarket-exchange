export interface IOrder {
	orderId?: number
	amount: number
	price: number
	profitLimit: number
	stopNear: number
	refPrice: number
	stopFar: number
}
