export interface IBalance {
	free: number
	used: number
	total: number
}
