import { IExchange } from './exchange'
import config from '../config/default.json'

export interface IState {
	strategy: typeof config.strategy
	econobit: IExchange & typeof config.econobit
	binance: IExchange & typeof config.binance
}

export const state: IState = {
	strategy: config.strategy,
	econobit: {
		balances: {},
		...config.econobit
	},
	binance: {
		balances: {},
		...config.binance
	}
}
