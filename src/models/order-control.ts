import { IOrder } from './order'

export interface IOrderControl {
	bid?: IOrder
	ask?: IOrder
}
