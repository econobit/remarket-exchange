import Big from 'big.js'
import { onExit } from './functions/on-exit'
import { Binance } from './models/binance'
import { IDepth } from './models/depth'
import { Econobit } from './models/econobit'
import { IOrderControl } from './models/order-control'
import { state } from './models/state'

Big.RM = Big.roundDown
const orders: IOrderControl = {}
const econobit = new Econobit()
const binance = new Binance()

export const main = async () => {
	await Promise.all([
		binance.init(),
		econobit.init(),
	])
	const { slippage, spread, amount, fees } = state.strategy

	onExit({
		cleanupHandler: async (exitCode, signal) => {
			console.log({ exitCode, signal })
			const orderIds: number[] = []
			if (orders.bid?.orderId) {
				orderIds.push(orders.bid.orderId)
			}
			if (orders.ask?.orderId) {
				orderIds.push(orders.ask.orderId)
			}
			if (orderIds.length > 0) {
				await econobit.cancelOrders(orderIds)
			}
			return true
		}
	})

	econobit.onTrade = async (trade) => {
		if (trade.buyerOrderId === orders.bid?.orderId) {
			await binance.sendOrder({
				amount: trade.amount,
				price: orders.bid.refPrice,
				side: 'sell'
			})
		}
		if (trade.sellerOrderId === orders.ask?.orderId) {
			await binance.sendOrder({
				amount: trade.amount,
				price: orders.ask.refPrice,
				side: 'buy'
			})
		}
	}

	econobit.onAddOrder = (order) => {
		orders[order.side === 'buy' ? 'bid' : 'ask'].orderId = order.orderId
	}

	econobit.onUpdateOrder = async (order) => {
		orders[order.side === 'buy' ? 'bid' : 'ask'].amount = Big(order.amount).minus(order.filled).round(4).toNumber()
	}

	econobit.onDeleteOrder = async (orderId) => {
		switch (orderId) {
			case orders.bid?.orderId:
				delete orders.bid
				break
			case orders.ask?.orderId:
				delete orders.ask
				break
		}
	}

	binance.on('message', async (msg) => {
		try {
			const data: IDepth & { result: any } = JSON.parse(msg.toString())
			if (!data.bids || !data.asks) {
				if (data.result === null) {
					console.log('binance subscription successful.')
				} else {
					console.warn('msg not handled', data)
				}
				return
			}
			const bidAmount = Big(orders.bid?.amount || amount)
			const askAmount = Big(orders.ask?.amount || amount)

			const bidPrice = data.bids.reduce((prev, curr) => {
				if (prev.amount.eq(0)) {
					return prev
				}
				if (prev.amount.gt(curr[1])) {
					return {
						total: prev.total.plus(Big(curr[0]).mul(curr[1])),
						amount: prev.amount.minus(curr[1]),
					}
				}
				return {
					total: prev.total.plus(prev.amount.mul(curr[0])),
					amount: Big(0),
				}
			}, { total: Big(0), amount: bidAmount }).total.div(bidAmount).round(0)

			const askPrice = data.asks.reduce((prev, curr) => {
				if (prev.amount.eq(0)) {
					return prev
				}
				if (prev.amount.gt(curr[1])) {
					return {
						total: prev.total.plus(Big(curr[0]).mul(curr[1])),
						amount: prev.amount.minus(curr[1]),
					}
				}
				return {
					total: prev.total.plus(prev.amount.mul(curr[0])),
					amount: Big(0),
				}
			}, { total: Big(0), amount: askAmount }).total.div(askAmount).round(0)

			if (econobit.ready && binance.ready) {
				if (!orders.bid) {
					// create bid order
					const bid = {
						amount: bidAmount.round(4).toNumber(),
						price: bidPrice.div(Big(1).plus(spread)).round(0).toNumber(),
						profitLimit: bidPrice.div(Big(1).plus(spread).minus(fees)).round(0).toNumber(),
						stopNear: bidPrice.div(Big(1).plus(slippage)).round(0).toNumber(),
						refPrice: bidPrice.round(0).toNumber(),
						stopFar: bidPrice.mul(Big(1).plus(slippage)).round(0).toNumber(),
					}
					if (
						bid.price > 0 &&
						econobit.checkBalance('buy', bid.refPrice) &&
						binance.checkBalance('sell')
					) {
						orders.bid = bid
						console.table({ bid })
						econobit.sendOrder({
							amount: orders.bid.amount,
							price: orders.bid.price,
							side: 'buy',
						})
					}
				}

				if (!orders.ask) {
					// create ask order
					const ask = {
						amount: askAmount.round(4).toNumber(),
						price: askPrice.mul(Big(1).plus(spread)).round(0).toNumber(),
						profitLimit: askPrice.mul(Big(1).plus(spread).minus(fees)).round(0).toNumber(),
						stopNear: askPrice.mul(Big(1).plus(slippage)).round(0).toNumber(),
						refPrice: askPrice.round(0).toNumber(),
						stopFar: askPrice.div(Big(1).plus(slippage)).round(0).toNumber(),
					}
					if (
						ask.price > 0 &&
						econobit.checkBalance('sell') &&
						binance.checkBalance('buy', ask.refPrice)
					) {
						orders.ask = ask
						console.table({ ask })
						econobit.sendOrder({
							amount: orders.ask.amount,
							price: orders.ask.price,
							side: 'sell',
						})
					}
				}
			}

			if (econobit.ready) {
				if (orders.bid) {
					// check if have to cancel order
					if (bidPrice.lt(orders.bid.stopNear)) {
						console.log('cancel bid (near):', bidPrice.toNumber(), '<', orders.bid.stopNear)
						await econobit.cancelOrder(orders.bid.orderId)
						binance.ready = false
					} else if (bidPrice.gt(orders.bid.stopFar)) {
						console.log('cancel bid (far):', bidPrice.toNumber(), '>', orders.bid.stopFar)
						await econobit.cancelOrder(orders.bid.orderId)
						binance.ready = false
					}
				}

				if (orders.ask) {
					// check if have to cancel order
					if (askPrice.gt(orders.ask.stopNear)) {
						console.log('cancel ask (near):', askPrice.toNumber(), '>', orders.ask.stopNear)
						await econobit.cancelOrder(orders.ask.orderId)
						binance.ready = false
					} else if (askPrice.lt(orders.ask.stopFar)) {
						console.log('cancel bid (far):', askPrice.toNumber(), '>', orders.ask.stopFar)
						await econobit.cancelOrder(orders.ask.orderId)
						binance.ready = false
					}
				}
			}
		} catch (err) {
			console.error(err)
		}
	})

	console.log('running...')
}
